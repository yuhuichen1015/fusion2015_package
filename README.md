# README #

The codes and data for the paper:

Chen, Yu-Hui, et al. "[Parameter estimation in spherical symmetry groups.](http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=7001052)" *Signal Processing Letters, IEEE* 22.8 (2015): 1152-1155.

The repository contains the codes to generate the simulated data, perform the experiments and generate the figures in the paper. The codes are written in MATLAB.

### How to run the code ###
The codes are all in the "Code" subdirectory under this repository. Please navigate your MATLAB path to the Code directory or set the path variable *package_path* in "Initial.m" correctly.

To execute the whole experiment, please run the "Main.m" script. Some parameters can also be set in "Main.m".

### Contact Info ###

The repository is created and owned by **Yu-Hui Chen** @ University of Michigan from professor [Alfred Hero](http://web.eecs.umich.edu/~hero/)'s group.

If you have any question about the code, please contact me through:
*yuhuichen1015@gmail.com*

More information could be find on our lab's [wiki page](http://tbayes.eecs.umich.edu/start).