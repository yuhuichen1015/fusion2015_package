clear;
Initial;

%%% Load the operators
SymOperatorsSrp;

%% Test for 1 cluster parameter estimation.
% Load the testing data
Kappa = 80;
testSetIdx = 1;
input_file = [data_path 'ParsEstimation/VMF_FZ_K' int2str(Kappa) '.mat'];
load(input_file);
X = Samples_All{testSetIdx};

%%% Testing WastonEM
for t=1:100
    [Mu, Kappa, W, logL] = VMFEM_Hyper(X, Pm);
end
[cs, Mu_est] = SymDistance(MeanDir_All{testSetIdx}, Mu, Pm);
cs
Kappa
W
logL

% %% Test for 2 clusters parameter estimation.
% % Load the testing data
Kappa = 80;
testSetIdx = 1;
input_file = [data_path 'Clustering/VMF_FZ_2C_K' int2str(Kappa) '.mat'];
load(input_file);
X = Samples_All{testSetIdx};
Num_of_clusters = 2;

%%% Testing WastonEM
[Mu, Kappa, W, logL] = VMFEM_Hyper(X, Pm, 2);
if(W(1)<0.5)
    [cs1, Mu_est] = SymDistance(MeanDir_1_All{testSetIdx}, Mu(1,:), Pm);
    [cs2, Mu_est] = SymDistance(MeanDir_2_All{testSetIdx}, Mu(2,:), Pm);
    cs1
    cs2
else
    [cs1, Mu_est] = SymDistance(MeanDir_2_All{testSetIdx}, Mu(1,:), Pm);
    [cs2, Mu_est] = SymDistance(MeanDir_1_All{testSetIdx}, Mu(2,:), Pm);
    cs1
    cs2
end
Kappa
W
logL
