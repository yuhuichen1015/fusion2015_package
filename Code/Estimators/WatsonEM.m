function [Mu, Kappa, W, logL, CIdx] = WatsonEM(X, Pm, Num_of_clusters)

%%% Duplicate the Euler Angles
N = size(X,1);
p = size(X,2);
M = size(Pm,3);

% Precompute the xAp, yAp for invAp_fast
xDp=-35:0.1:500;
yDp = Dp(xDp, p);
    
if(nargin<3)
    Num_of_clusters=1;
end
Num_of_init = 5;

Mu_All = zeros(Num_of_clusters, p, Num_of_init);
Kappa_All = zeros(Num_of_clusters, Num_of_init);
W_All = zeros(Num_of_clusters, Num_of_init);
R_All = cell(Num_of_init,1);
L_All = zeros(Num_of_init,1);

trial = 1;
while(trial<=Num_of_init)
    % Initialize Mu and Kappa
    R = zeros(N,M,Num_of_clusters);
    Mu = zeros(Num_of_clusters, p);
    for clu = 1:Num_of_clusters
        mu = randn(1,p);
        mu = mu/norm(mu,2);
        Mu(clu,:) = mu(:)';
    end
    Kappa = 100*ones(Num_of_clusters,1);
    W = (1/Num_of_clusters)*ones(Num_of_clusters,1);
    Alpha = 1/M*ones(M,1);
    
    Num_of_iteration=30;
    Q=zeros(Num_of_iteration,1);
    L=zeros(Num_of_iteration,1);
    isfound = true;
    for ite=1:Num_of_iteration
        %%% E-step
        for clu = 1:Num_of_clusters
            mu = Mu(clu,:);
            kappa = Kappa(clu);
            w = W(clu);
            for j=1:M
                R(:,j,clu) = w*Alpha(j)*WatsonDensity(X, Pm(:,:,j)*mu(:), kappa)+eps;
            end
        end
        % Normalization
        Rdenom = sum(sum(R,3),2);
        R = R ./ repmat(Rdenom, [1,M,Num_of_clusters]);

        % M-step
        % estimate W
        W = squeeze(sum(sum(R,1),2));
        W = W / sum(W(:));
            
        if(any(W<0.001))
            isfound=false;
            warning('unbalanced result');
            break;
        end
        %%% Estimate mu
        for clu = 1:Num_of_clusters
            % Calculate Scatter Matrix T
            T = zeros(p,p);
            for j=1:M
                rX = repmat(sqrt(R(:,j,clu)), [1,p]).*X;
                rXXT = rX'*rX;
                T = T + Pm(:,:,j)'*rXXT*Pm(:,:,j);
            end
            T = T / (N*W(clu));
            [V, D] = eig(T);
            lambda = diag(D);
            [yy, dd] = sort(lambda, 'descend');
            
            Mu_1 = V(:,dd(1));
            Mu_p = V(:,dd(end));
            
            %%% Estimate Kappa
            TT = Mu_1(:)'*T*Mu_1(:);
            Kappa_tmp = invDp(TT, xDp, yDp);
            Mu_tmp = Mu_1;
            if(Kappa_tmp < 0)
                %%% The other case, need to use Mu_p
                TT = Mu_p(:)'*T*Mu_p(:);
                Kappa_tmp = invDp(TT, xDp, yDp);
                Mu_tmp = Mu_p;
                if (Kappa_tmp > 0)
                    warning('Kappa estimation condition is violated.');
                    Kappa = zeros(Num_of_clusters,1);
                    Mu = zeros(Num_of_clusters, p);
                    isfound = false;
                    break;
                end
            end  
            Mu(clu,:) = Mu_tmp(:)';
            Kappa(clu) = Kappa_tmp;
        end
        if(~isfound)
            break;
        end
       
        Kappa(Kappa>710) = 710;
        % Calculate the Q function
        Phi = zeros(N,M,Num_of_clusters);
        for clu = 1:Num_of_clusters
            for j=1:M
                Phi(:,j,clu) = W(clu)*Alpha(j)*WatsonDensity(X, Pm(:,:,j)*Mu(clu,:)', Kappa(clu));
            end
        end
        
        L(ite) = sum(log(sum(sum(Phi,3),2)));
        Q(ite) = sum(sum(sum(R.*log(Phi+eps))));
       
        if(ite>=2)
            if(abs(Q(ite)-Q(ite-1))<0.05)
                break;
            end
        end
    end
    if(isfound)
        % update the containers
        Mu_All(:,:,trial) = Mu;
        Kappa_All(:,trial) = Kappa(:);
        W_All(:,trial) = W(:);
        R_All{trial} = R;
        L_All(trial) = L(ite);
        trial = trial + 1;
    end
end

[yy, dd] = max(L_All);
Mu=Mu_All(:,:,dd);
Kappa=Kappa_All(:,dd);
W = W_All(:,dd);
logL = L_All(dd);
if(Num_of_clusters>1)
        R = R_All{dd}; 
        [yy,CIdx] = max(squeeze(sum(R,2)),[],2);
    else
        CIdx = ones(N,1);
end
end