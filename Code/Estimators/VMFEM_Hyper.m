function [Mu, Kappa, W, logL, CIdx]=VMFEM_Hyper(X, Pm, Num_of_clusters)
    %%% Duplicate the Euler Angles
    No = size(Pm,3);
    N = size(X,1);
    p = size(X,2);
    
    % Precompute the xAp, yAp for invAp_fast
    xAp=0.001:0.1:700;
    yAp = Ap(xAp, p);
    
    % Create container for estimated parameters
    if(nargin<3)
        Num_of_clusters=1;
    end
    Num_of_init=5;
    Mu_All = zeros(Num_of_clusters, p, Num_of_init);
    Kappa_All = zeros(Num_of_clusters, Num_of_init);
    W_All = zeros(Num_of_clusters, Num_of_init);
    R_All = cell(Num_of_init,1);
    L_All = zeros(Num_of_init,1);
    for init=1:Num_of_init   
        % Initialize Mu and Kappa
        Mu = zeros(Num_of_clusters, p);
        for clu = 1:Num_of_clusters
            mu = randn(1,p);
            mu = mu/norm(mu,2);
            Mu(clu,:) = mu(:)';
        end
        
        Kappa = 50*ones(Num_of_clusters,1);
        W = (1/Num_of_clusters)*ones(Num_of_clusters,1);
                
%         Mu = [0.3465   -0.7765    0.2225    0.4770; 0 -1/sqrt(2) -1/sqrt(2) 0;];
%         Mu(1,:) = Mu(1,:) / norm(Mu(1,:),2);
%         Mu(2,:) = Mu(2,:) / norm(Mu(2,:),2);
%         W = [0.7 0.3];
%         Kappa = [80 80];
        
        Num_of_iteration=30;
        L=zeros(Num_of_iteration,1);
        for ite=1:Num_of_iteration
            %%% E-step
            SinH = zeros(N, No, Num_of_clusters);
            CosH = zeros(N, No, Num_of_clusters);
            for clu = 1:Num_of_clusters
                % E-step     
                for j=1:No
                    CosH(:,j,clu) = W(clu)*Cp(Kappa(clu),p)*cosh(Kappa(clu)*X*(Pm(:,:,j)*Mu(clu,:)'));
                    SinH(:,j,clu) = W(clu)*Cp(Kappa(clu),p)*sinh(Kappa(clu)*X*(Pm(:,:,j)*Mu(clu,:)'));
                end
            end
            R = SinH ./ repmat(sum(sum(CosH,3),2), [1 No Num_of_clusters]);
            
            %%% M-step
            % estimate W
            CosHSum = squeeze(sum(CosH,2));
            CosHSumNorm = CosHSum ./ repmat(sum(CosHSum,2), [1, Num_of_clusters]);
            W = sum(CosHSumNorm,1)';
            W = W / sum(W(:));
            for clu = 1:Num_of_clusters
                % estimate Mu
                tmpGamma = zeros(No, p);
                for j=1:No
                    tmpGamma(j,:) = sum((Pm(:,:,j)'*X')'.*repmat(R(:,j,clu), [1 4]));
                end
                Gamma = sum(tmpGamma,1);
                Mu(clu,:) = Gamma / norm(Gamma,2);
                % estimate Kappa
                Kappa(clu) = invAp(norm(Gamma,2)/(W(clu)*N), p, xAp, yAp);
            end
            
            PhiT = zeros(N,No,Num_of_clusters);
            for clu = 1:Num_of_clusters
                for j=1:No
                    PhiT(:,j,clu) = 2*W(clu)*Cp(Kappa(clu),p)*cosh(Kappa(clu)*X*(Pm(:,:,j)*Mu(clu,:)'));
                end
            end
            L(ite) = sum(log(sum(sum(PhiT+eps,3),2)));
            
            
            % update the containers
            Mu_All(:,:,init) = Mu;
            Kappa_All(:,init) = Kappa(:);
            W_All(:,init) = W(:); 
            R_All{init} = R;
            L_All(init) = L(ite);
            
            % Check stopping criteria
            if(ite>=2)
                if(abs(L(ite)-L(ite-1))<0.05)
                    break;
                end
            end
        end   
    end
    [yy, dd] = max(L_All);
    Mu=Mu_All(:,:,dd);
    Kappa=Kappa_All(:,dd);
    W = W_All(:,dd);
    logL = L_All(dd);
    if(Num_of_clusters>1)
        R = R_All{dd}; 
        [yy,CIdx] = max(squeeze(sum(R,2)),[],2);
    else
        CIdx = ones(N,1);
    end
end