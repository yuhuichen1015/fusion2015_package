function [Mu_est, Kappa_est] = OriginEstforWatson(Samples)
    p = size(Samples,2);
    N = size(Samples,1);
    
    X=Samples;
    T = X'*X/N;
    [V, D] = eig(T);
    lambda = diag(D);
    [yy, dd] = sort(lambda, 'descend');
    
    Mu_1 = V(:,dd(1));
    Mu_p = V(:,dd(end));
    
    %%% Estimate Kappa
    TT = Mu_1(:)'*T*Mu_1(:);
    Kappa_est = (1/2)*((1-TT*p)/(TT^2-TT)) - TT^2 / (p*(TT^2-TT));
    Mu_est = Mu_1;
    if(Kappa_est < 0)
        %%% The other case, need to use Mu_p
        TT = Mu_p(:)'*T*Mu_p(:);
        Kappa_est = (1/2)*((1-TT*p)/(TT^2-TT)) - TT^2 / (p*(TT^2-TT));
        Mu_est = Mu_p;
        if (Kappa > 0)
            warning('Kappa estimation condition is violated.');
        end
    end
end