function [y] = WatsonDensity(x, mu, k)
    p = size(x,2);
    if(p~=size(mu(:),1))
        error('The dimension of samples should be the same as mean direction.');
    end
    C = 1/kummer(1/2,p/2,k);
    ytmp = log(C) + k*(x*mu(:)).^2;
    y = exp(ytmp);
%     y = C*exp(k*(x*mu(:)).^2);
end