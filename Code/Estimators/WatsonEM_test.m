clear;
Initial;

%%% Load the operators
SymOperatorsSrp;

%% Test for 1 cluster parameter estimation.
% Load the testing data
Kappa = 80;
testSetIdx = 1;
input_file = [data_path 'ParsEstimation/Watson_FZ_K' int2str(Kappa) '.mat'];
load(input_file);
X = Samples_All{testSetIdx};

%%% Testing WastonEM
[Mu, Kappa] = WatsonEM(X, Pm);
[cs, Mu_est] = SymDistance(MeanDir_All{testSetIdx}, Mu, Pm);
cs
Kappa

%% Test for 2 clusters parameter estimation.
% Load the testing data
Kappa = 80;
testSetIdx = 1;
input_file = [data_path 'Clustering/Watson_FZ_2C_K' int2str(Kappa) '.mat'];
load(input_file);
X = Samples_All{testSetIdx};

%%% Testing WastonEM
[Mu, Kappa, W] = WatsonEM(X, Pm, 2);
if(W(1)<0.5)
    [cs1, Mu_est] = SymDistance(MeanDir_1_All{testSetIdx}, Mu(1,:), Pm);
    [cs2, Mu_est] = SymDistance(MeanDir_2_All{testSetIdx}, Mu(2,:), Pm);
    cs1
    cs2
else
    [cs1, Mu_est] = SymDistance(MeanDir_2_All{testSetIdx}, Mu(1,:), Pm);
    [cs2, Mu_est] = SymDistance(MeanDir_1_All{testSetIdx}, Mu(2,:), Pm);
    cs1
    cs2
end
Kappa
