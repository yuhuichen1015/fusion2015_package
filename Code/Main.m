clear;
Initial;

%%
%%% The simulation experiment process for 1 cluster parameter estimation. 
%%% Related to Figure 1,2,3 in the paper.
%%% Option setting:
% Overwrite the generated data if they exist.
overwrite_generation = true; 
% Overwrite the result data if they exist.
overwrite_computation = false; 
% Determine the kappa value we want to generate.
KappaSet = [1:100];
% Determine the number of sets for each kappa.
Num_of_sets = 100;
% Determine the number of samples per set.
Num_of_samples = 1000;

%%% Simulation experiment execution for 1 cluster parameter estimation.
SimulationParsEstimationExp;

%%
%%% The simulation experiment process for clustering. 
%%% Related to Figure 4 in the paper.
%%% Option setting:
% Overwrite the generated data if they exist.
overwrite_generation = false; 
% Overwrite the result data if they exist.
overwrite_computation = false; 
% Determine the kappa value we want to generate.
KappaSet = [1:100];
% Determine the number of sets for each kappa.
Num_of_sets = 100;
% Determine the number of samples per set.
Num_of_samples = 1000;

%%% Simulation experiment execution for 1 cluster parameter estimation.
SimulationClusteringExp;

%%
%%% The simulation experiment process for hypothesis test. 
%%% Related to Figure 5 in the paper.
%%% Option setting:
% Overwrite the generated data if they exist.
overwrite_generation = false; 
% Overwrite the result data if they exist.
overwrite_computation = false; 
% Determine the kappa value we want to generate.
KappaSet = [50];
% Determine the number of sets for each kappa.
Num_of_sets = 1000;
% Determine the number of samples per set.
Num_of_samples = 1000;

%%% Simulation experiment execution for 1 cluster parameter estimation.
SimulationHypoTestExp;

%%
%%% Real data option setting
% Overwrite the generated data and result even if they exist.
overwrite_computation = false;

% Define the selected region to show
Selected_pts = [1, 184];
Height = 200;
Width = 200;

%%% Real data experiment execution
RealDataExp;