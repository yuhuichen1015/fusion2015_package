function [Boundary] = imgIdx2Boundary(Indexing)
% Enlarge the bounding box by padding zero
TmpIndexing = zeros([size(Indexing,1)+10, size(Indexing,2)+10]);
TmpIndexing(6:end-5, 6:end-5) = Indexing;
Indexing = TmpIndexing;
Sz = size(Indexing);

S=max(Indexing(:));
Boundary = zeros(size(Indexing));
for s=1:S
    tmpBnd=(Indexing==s);
    BW=bwboundaries(tmpBnd,'noholes');
    if(~isempty(BW))
        L=zeros(size(BW,1));
        for i=1:size(BW,1)
            L(i) = size(BW{i},1);
        end
        [yy dd] = max(L);
        bdry = BW{dd};
        sz_b = size(bdry,1);
        for i=1:sz_b
            Boundary(bdry(i,1), bdry(i,2)) = 1;
        end
    end
end
% The outer part
tmpO = Indexing;
tmpO(Indexing>0)=1;
tmpO(Indexing==0)=0;
Gr = imdilate(tmpO,strel('disk',1));
BW = bwboundaries(Gr);
L=[];
for i=1:size(BW,1)
    L=[L; size(BW{i},1)];
end
[yy, dd] = max(L);
bdry = BW{dd(1)};
ii = bdry(:,1)+Sz(1)*(bdry(:,2)-1);
Boundary(ii) = 1;
Boundary = Boundary(6:end-5, 6:end-5);
end