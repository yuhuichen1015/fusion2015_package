function [QuatT] = TransformToRef(QuatRef, Quat, Pm)
    QuatRef = QuatRef(:)';
    QuatT = zeros(size(Quat));
    N = size(Quat,1);
    
    % Transforming all angles to the be closest to the first one
    for i=1:N
        sym = zeros(24,1);  
        for j=1:24
            sym(j) = (QuatRef*(Pm(:,:,j)'*Quat(i,:)'));
        end
        [cs, dd] = max(abs(sym));
        if(sym(dd)<0)
            QuatT(i,:) = -(Pm(:,:,dd)'*Quat(i,:)')';
        else
            QuatT(i,:) = (Pm(:,:,dd)'*Quat(i,:)')';
        end
    end
end