a = [ 0.2157   -1.1658   -1.1480    0.1049];
a = a / norm(a);
b = [0.7223    2.5855   -0.6669    0.1873];
b = b / norm(b);
X = [repmat(a, [100,1]); repmat(b, [100,1]);];

% [idx,ctrs] = mykmeans(X,2,'Distance','cosine','Replicates',5);


[idx,ctrs] = mykmeans(X,2,'Distance','SymMetric','Replicates',5);

[Dist, X_Sym] = SymMetric(X, ctrs(2,:));

Y = zeros(size(X));
for i=1:200
    ri = randi(24);
    Y(i,:) = (Pm(:,:,ri)'*X(i,:)')';
end


[idx,ctrs] = mykmeans(Y,2,'Distance','SymMetric','Replicates',5);

[Dist, X_Sym] = SymMetric(Y, ctrs(2,:)/norm(ctrs(2,:)));

