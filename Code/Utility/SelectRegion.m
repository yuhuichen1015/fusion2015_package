function [I] = SelectRegion(I, Selected_pts, Height, Width) 
    tmpI = zeros(Height, Width, size(I,3));
    for i=1:size(I,3)
        tmpI(:,:,i) = I(Selected_pts(1):Selected_pts(1)+Height-1,...
                        Selected_pts(2):Selected_pts(2)+Width-1,i);
    end
    I=tmpI;
end