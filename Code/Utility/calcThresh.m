function [threshold] = calcThresh(A, sig_d, alpha)
%   This function is used by hypoTestOnBimodalState in testMode==3 only. It
%   calculate the threshold for the ratio between the number of pixels of
%   population 1 and 2. It is the test to differentiate the difference
%   between misalignment and missing edge. Basically the misalignment is
%   modeled by considering the grain moement which follows the Gaussian
%   distribution.
% 
% inputs,
%   A : It is a scalar which is the total area of the current grain (in
%           pixel.
%
%   sig_d : It is a scalar which is the sigma of the Gaussian distribution.
%
%   alpha : It is the False Discovery rate of this hypothesis test.      
%
% outputs,
%   threshold : It is threshold of the ratio between the number of pixels
%           of two populations. It has the value between 0~1.
%
% Example, See 'DemoBimodalityTest.m'
%
% Function is written by Yu-Hui Chen, University of Michigan

eta = sqrt(-2*sig_d^2*log(alpha));
r = sqrt(A/pi);
threshold = 1 - 2/pi*acos(eta/(2*r)) + eta/(pi*r^2)*sqrt(r^2-eta^2/4);
if(threshold>0.5)
    threshold = 1;
end
    
end