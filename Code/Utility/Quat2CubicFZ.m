function [QuatFZ, OptrIdx] = Quat2CubicFZ(Quat)

% This function convert the quaternions into pre-defined fundamental zone
% for cubic symmetry.
% 
% Usage:
%  [QuatFZ, OptrIdx] = Quat2CubicFZ(Quat)
% 
% inputs,
%   Quat : A 4 X N matrix which contains the N quaternions that need to be
%           converted into fundamental zone. 
%   
%  
% outputs,
%   QuatFZ : A 4 x N matrix which contains the N quaternions that are in the 
%           fundamental zone. 
% 
%   OptrIdx : A 1 X N matrix which saves the indices of operators it use to
%           convert the quaternions into fundamental zone. 
%  
% Function is written by Yu-Hui Chen, University of Michigan

%%% Generating the operators for cubic symmetry
SymOperators= [ 0.000000000, 0.000000000, 0.000000000, 1.000000000;
                1.000000000, 0.000000000, 0.000000000, 0.000000000;
                0.000000000, 1.000000000, 0.000000000, 0.000000000;
                0.000000000, 0.000000000, 1.000000000, 0.000000000;
                0.707106781, 0.000000000, 0.000000000, 0.707106781;
                0.000000000, 0.707106781, 0.000000000, 0.707106781;
                0.000000000, 0.000000000, 0.707106781, 0.707106781;
                -0.707106781, 0.000000000, 0.000000000, 0.707106781;
                0.000000000, -0.707106781, 0.000000000, 0.707106781;
                0.000000000, 0.000000000, -0.707106781, 0.707106781;
                0.707106781, 0.707106781, 0.000000000, 0.000000000;
                -0.707106781, 0.707106781, 0.000000000, 0.000000000;
                0.000000000, 0.707106781, 0.707106781, 0.000000000;
                0.000000000, -0.707106781, 0.707106781, 0.000000000;
                0.707106781, 0.000000000, 0.707106781, 0.000000000;
                -0.707106781, 0.000000000, 0.707106781, 0.000000000;
                0.500000000, 0.500000000, 0.500000000, 0.500000000;
                -0.500000000, -0.500000000, -0.500000000, 0.500000000;
                0.500000000, -0.500000000, 0.500000000, 0.500000000;
                -0.500000000, 0.500000000, -0.500000000, 0.500000000;
                -0.500000000, 0.500000000, 0.500000000, 0.500000000;
                0.500000000, -0.500000000, -0.500000000, 0.500000000;
                -0.500000000, -0.500000000, 0.500000000, 0.500000000;
                0.500000000, 0.500000000, -0.500000000, 0.500000000;];
            
            
SymOptr=zeros(4,4,24);
for i=1:24
    c0=SymOperators(i,1); c1=SymOperators(i,2); c2=SymOperators(i,3); c3=SymOperators(i,4);
    SymOptr(:,:,i)=[ c3  c2 -c1  c0;
               -c2  c3  c0  c1;
                c1 -c0  c3  c2;
               -c0 -c1 -c2  c3;];
end

%%% Start to convert the quaternions into fundamental zone
Sz = size(Quat);
if(Sz(1)~=4)
    error('wrong inputs');
end
QuatFZ = zeros(Sz);
OptrIdx = zeros(size(Quat,2),1);
for i=1:Sz(2) % Loop through each input quaternion
    repRod = zeros(3,24);
    inFZ = zeros(1,24);
    for j=1:24 % replicate the quaternion by the 24 operators
        q = SymOptr(:,:,j)*Quat(:,i);
        repRod(:,j) = [q(1)/q(4); q(2)/q(4); q(3)/q(4)];
    end
    for j=1:24 % Loop through each replicates to check which one is in FZ
        r1=repRod(1,j);
        r2=repRod(2,j);
        r3=repRod(3,j);
        % The inequalities which defined the FZ
        if(abs(r1)<=sqrt(2)-1 && abs(r2)<=sqrt(2)-1 && abs(r3)<=sqrt(2)-1)
            %if(abs(r1-r2)<=sqrt(2) && abs(r1+r2)<=sqrt(2) && abs(r1-r3)<=sqrt(2) && abs(r1+r3)<=sqrt(2) && abs(r2-r3)<=sqrt(2) && abs(r2+r3)<=sqrt(2))
                if(abs(r1+r2+r3)<=1 && abs(r1-r2+r3)<=1 && abs(r1+r2-r3)<=1 && abs(-r1+r2+r3)<=1)
                    inFZ(j)=1;
                end
            %end
        end
    end
    if(sum(inFZ)~=1) % If there is not exactly one replicate in the FZ
        repRod
        Quat(:,i)
        inFZ
        error('something wrong happens');
    end
    % Convert Rodrigues vec back to quaternion
    R = repRod(:,find(inFZ));
    OptrIdx(i) = find(inFZ);
    q=zeros(4,1);
    normR2 = norm(R,2)^2;
    q(4) = 1/sqrt(1+normR2);
    q(1) = R(1)/sqrt(1+normR2);
    q(2) = R(2)/sqrt(1+normR2);
    q(3) = R(3)/sqrt(1+normR2);
    QuatFZ(:,i) = q;
end

end