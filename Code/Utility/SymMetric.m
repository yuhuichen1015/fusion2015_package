function [Dist, X_Sym] = SymMetric(X, q)
    SymOperatorsSrp;
    [N, p] = size(X);
    X_Sym = zeros(size(X));
    if(p~=4)
        error('dimension should be 4');
    end
    DistMtx = zeros(N, size(Pm,3));
    for i=1:size(Pm,3)
        DistMtx(:,i) = acos(min(abs(X*(Pm(:,:,i)'*q(:))),1));
    end
    [Dist, Idx] = min(DistMtx,[],2);
    for i=1:N
        X_Sym(i,:) = (Pm(:,:,Idx(i))*X(i,:)')';
        if(X_Sym(i,:)*q(:)<0)
            X_Sym(i,:) = -X_Sym(i,:);
        end
    end
end