function [cs, Mu_est] = SymDistance(RefDir, Mu, Pm)
    sym = zeros(24,1);
    for i=1:24
        sym(i) = abs(RefDir(:)'*(Pm(:,:,i)'*Mu(:)));
    end
    [cs dd] = max(sym);
    if(RefDir(:)'*(Pm(:,:,dd)'*Mu(:))<0)
        Mu_est = -Pm(:,:,dd)'*Mu(:);
    else
        Mu_est = Pm(:,:,dd)'*Mu(:);
    end
end