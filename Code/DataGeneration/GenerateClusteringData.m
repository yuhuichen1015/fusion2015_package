% Call the script to define the cubic symmetry operators.
SymOperatorsSrp;
mkdir([data_path 'Clustering/']);

%% Generate samples from VMF distribution
for Kappa = KappaSet  
    ['Generating VMF samples for kappa = ' int2str(Kappa)]
    filename = [data_path 'Clustering/VMF_FZ_2C_K' int2str(Kappa) '.mat'];
    if(exist(filename, 'file') && ~overwrite_generation)
        warning(['file:' filename ' exists. Generation skipped.']);
        continue;
    end

    Kappa_All = cell(Num_of_sets, 1);
    CIdx_All = cell(Num_of_sets, 1);
    StateVar_All = cell(Num_of_sets, 1);
    MeanDir_1_All = cell(Num_of_sets, 1);
    MeanDir_2_All = cell(Num_of_sets, 1);
    Samples_All = cell(Num_of_sets, 1);
    for sets = 1:Num_of_sets                   
        W = 0.3;
        n1 = round(Num_of_samples*W);
        n2 = Num_of_samples - n1;
        
        Kappa_All{sets} = [Kappa, Kappa];
        cidx = [ones(n1,1); 2*ones(n2,1)];
        CIdx_All{sets} = cidx;
        
        % Generate the main direction
        [RanSphere] = randUniformSphere(1, 4);
        MeanDir_1_All{sets} = RanSphere;
        [RanSphere] = randUniformSphere(1, 4);
        MeanDir_2_All{sets} = RanSphere;
        
        [RandVMF1] = randVMF(n1, MeanDir_1_All{sets}, Kappa);
        [RandVMF2] = randVMF(n2, MeanDir_2_All{sets}, Kappa);
                
        RandVMF = [RandVMF1; RandVMF2];
        [QuatFZ, OptrIdx] = Quat2CubicFZ(RandVMF');
        
        StateVar_All{sets} = OptrIdx;
        Samples_All{sets} = QuatFZ';
    end  
    save(filename, 'Kappa_All', 'StateVar_All', 'MeanDir_1_All', 'MeanDir_2_All', 'CIdx_All', 'Samples_All');  
end

%% Generate samples from Watson distribution
for Kappa = KappaSet  
    ['Generating Watson samples for kappa = ' int2str(Kappa)]
    filename = [data_path 'Clustering/Watson_FZ_2C_K' int2str(Kappa) '.mat'];
    if(exist(filename, 'file') && ~overwrite_generation)
        warning(['file:' filename ' exists. Generation skipped.']);
        continue;
    end

    Kappa_All = cell(Num_of_sets, 1);
    CIdx_All = cell(Num_of_sets, 1);
    StateVar_All = cell(Num_of_sets, 1);
    MeanDir_1_All = cell(Num_of_sets, 1);
    MeanDir_2_All = cell(Num_of_sets, 1);
    Samples_All = cell(Num_of_sets, 1);
    for sets = 1:Num_of_sets                   
        W = 0.3;
        n1 = round(Num_of_samples*W);
        n2 = Num_of_samples - n1;
        
        Kappa_All{sets} = [Kappa, Kappa];
        cidx = [ones(n1,1); 2*ones(n2,1)];
        CIdx_All{sets} = cidx;
        
        % Generate the main direction
        [RanSphere] = randUniformSphere(1, 4);
        MeanDir_1_All{sets} = RanSphere;
        [RanSphere] = randUniformSphere(1, 4);
        MeanDir_2_All{sets} = RanSphere;
        
        [RandWatson1] = randWatson(n1, MeanDir_1_All{sets}, Kappa);
        [RandWatson2] = randWatson(n2, MeanDir_2_All{sets}, Kappa);
                
        RandWatson = [RandWatson1; RandWatson2];
        [QuatFZ, OptrIdx] = Quat2CubicFZ(RandWatson');
        
        StateVar_All{sets} = OptrIdx;
        Samples_All{sets} = QuatFZ';
    end  
    save(filename, 'Kappa_All', 'StateVar_All', 'MeanDir_1_All', 'MeanDir_2_All', 'CIdx_All', 'Samples_All');  
end

