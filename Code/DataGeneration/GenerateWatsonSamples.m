clear;
addpath('../RandomSampleFromSphere');
addpath('../EulerSymmetry');
SymOperatorsSrp;

data_folder = ['./Data/'];

%%% Generate sample by fundamental zone
for kkk = 1:700
    save_filename = ['TestingWatson_SmallSize_FZ_K' int2str(kkk) '.mat'];
    
    Num_of_sets = 100;
    Num_of_samples = 40;
    Kappa = kkk;
    StateVar_All = cell(Num_of_sets, 1);
    MeanDir_All = cell(Num_of_sets, 1);
    Samples_All = cell(Num_of_sets, 1);
    
    for sets = 1:Num_of_sets
        kkk
        sets                      
        % Generate the main direction
        [RanSphere] = randUniformSphere(1, 4);
        MeanDir_All{sets} = RanSphere;
        
        [RandWatson] = randWatson(Num_of_samples, RanSphere, Kappa);
%         cidx = RanSphere*RandWatson';
%         DisplayQuat(RandWatson', 3, cidx);
        
        [QuatFZ, OptrIdx] = Quat2CubicFZ(RandWatson');
%          DisplayQuat(QuatFZ, 3, cidx);
        
        StateVar_All{sets} = OptrIdx;
        Samples_All{sets} = QuatFZ';
    end    
    save([data_folder save_filename], 'Kappa', 'StateVar_All', 'MeanDir_All', 'Samples_All');
end
