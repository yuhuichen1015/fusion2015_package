% Copmare the parameter estimation performance for different methods:
% 1. Maximum likelihood estimator
% 2. Modified ML estimator
% 3. EM for VMF estimator
% 4. EM for Watson estimator
% 5. EM for VMF Hyper estimator

Methods = {'Ori', 'Cls', 'VMF', 'Watson'};
mkdir([result_path 'Clustering/']);
SymOperatorsSrp;

%% Compare the result by samples generated from VMF.
for Kappa=KappaSet
    ['Computing the result for kappa = ' int2str(Kappa)]
    %%% Testing EM algorithm
    input_file = [data_path 'Clustering/VMF_FZ_2C_K' int2str(Kappa) '.mat'];
    load(input_file);

    for mtd = 1:size(Methods(:),1)
        method = Methods{mtd};
        result_file = [result_path 'Clustering/VMF_FZ_2C_K' int2str(Kappa) '_' method '_Result.mat'];
        if(exist(result_file, 'file') && ~overwrite_computation)
            warning(['Result for kappa = ' int2str(Kappa) ' exists. Computation skipped']);
            continue;
        end
        
        Num_of_sets = size(MeanDir_1_All,1);
        Cosin_All = zeros(Num_of_sets,1);
        Kappa_All = zeros(Num_of_sets,2);  
        for s=1:Num_of_sets
            Samples = Samples_All{s};
            switch(method)
                case 'Ori' % Origin ML estimator with kmeans
                    [idx,ctrs] = mykmeans(Samples,2,'Distance','cosine','Replicates',5);
                    X1 = Samples(idx==1,:);
                    X2 = Samples(idx==2,:);
                    [Mu1_est, Kappa1_est] = OriginEstforVMF(X1);
                    [Mu2_est, Kappa2_est] = OriginEstforVMF(X2);
                    W_est = sum(idx==1)/size(idx(:),1);
                    if(W_est<0.5)
                        [cs1, Mu1_est] = SymDistance(MeanDir_1_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_2_All{s}, Mu2_est, Pm);
                    else
                        [cs1, Mu1_est] = SymDistance(MeanDir_2_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_1_All{s}, Mu2_est, Pm);
                    end
                case 'Cls'
                    [idx,ctrs] = mykmeans(Samples,2,'Distance','SymMetric','Replicates',5);
                    X1 = Samples(idx==1,:);
                    X2 = Samples(idx==2,:);
                    [Mu1_est, Kappa1_est] = CloestEstforVMF(X1, Pm);
                    [Mu2_est, Kappa2_est] = CloestEstforVMF(X2, Pm);
                    W_est = sum(idx==1)/size(idx(:),1);
                    if(W_est<0.5)
                        [cs1, Mu1_est] = SymDistance(MeanDir_1_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_2_All{s}, Mu2_est, Pm);
                    else
                        [cs1, Mu1_est] = SymDistance(MeanDir_2_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_1_All{s}, Mu2_est, Pm);
                    end
                case 'VMF'
                    %%% Estimate by EM
                    [Mu_est, Kappa_est, W_est]=VMFEM_Hyper(Samples, Pm, 2);
                    Mu1_est = Mu_est(1,:);
                    Mu2_est = Mu_est(2,:);
                    Kappa1_est = Kappa_est(1);
                    Kappa2_est = Kappa_est(2);
                    if(W_est(1)<0.5)
                        [cs1, Mu1_est] = SymDistance(MeanDir_1_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_2_All{s}, Mu2_est, Pm);
                    else
                        [cs1, Mu1_est] = SymDistance(MeanDir_2_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_1_All{s}, Mu2_est, Pm);
                    end
                case 'Watson'
                    %%% Estimate by Watson EM
                    [Mu_est, Kappa_est, W_est]=WatsonEM(Samples, Pm, 2);
                    Mu1_est = Mu_est(1,:);
                    Mu2_est = Mu_est(2,:);
                    Kappa1_est = Kappa_est(1);
                    Kappa2_est = Kappa_est(2);
                    if(W_est(1)<0.5)
                        [cs1, Mu1_est] = SymDistance(MeanDir_1_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_2_All{s}, Mu2_est, Pm);
                    else
                        [cs1, Mu1_est] = SymDistance(MeanDir_2_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_1_All{s}, Mu2_est, Pm);
                    end
            end
            Cosin_All(s) = (cs1+cs2)/2;
            Kappa_All(s,:) = [Kappa1_est, Kappa2_est];
        end
        save(result_file,'Cosin_All', 'Kappa_All');
    end
end

%% Compare the result by samples generated from Watson.
for Kappa=KappaSet
    ['Computing the result for kappa = ' int2str(Kappa)]
    %%% Testing EM algorithm
    input_file = [data_path 'Clustering/Watson_FZ_2C_K' int2str(Kappa) '.mat'];
    load(input_file);

    for mtd = 1:size(Methods(:),1)
        method = Methods{mtd};
        result_file = [result_path 'Clustering/Watson_FZ_2C_K' int2str(Kappa) '_' method '_Result.mat'];
        if(exist(result_file, 'file') && ~overwrite_computation)
            warning(['Result for kappa = ' int2str(Kappa) ' exists. Computation skipped']);
            continue;
        end
        
        Num_of_sets = size(MeanDir_1_All,1);
        Cosin_All = zeros(Num_of_sets,1);
        Kappa_All = zeros(Num_of_sets,2);  
        for s=1:Num_of_sets
            Samples = Samples_All{s};
            switch(method)
                case 'Ori' % Origin ML estimator with kmeans
                    [idx,ctrs] = mykmeans(Samples,2,'Distance','cosine','Replicates',5);
                    X1 = Samples(idx==1,:);
                    X2 = Samples(idx==2,:);
                    [Mu1_est, Kappa1_est] = OriginEstforVMF(X1);
                    [Mu2_est, Kappa2_est] = OriginEstforVMF(X2);
                    W_est = sum(idx==1)/size(idx(:),1);
                    if(W_est<0.5)
                        [cs1, Mu1_est] = SymDistance(MeanDir_1_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_2_All{s}, Mu2_est, Pm);
                    else
                        [cs1, Mu1_est] = SymDistance(MeanDir_2_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_1_All{s}, Mu2_est, Pm);
                    end
                case 'Cls'
                    [idx,ctrs] = mykmeans(Samples,2,'Distance','SymMetric','Replicates',5);
                    X1 = Samples(idx==1,:);
                    X2 = Samples(idx==2,:);
                    [Mu1_est, Kappa1_est] = CloestEstforVMF(X1, Pm);
                    [Mu2_est, Kappa2_est] = CloestEstforVMF(X2, Pm);
                    W_est = sum(idx==1)/size(idx(:),1);
                    if(W_est<0.5)
                        [cs1, Mu1_est] = SymDistance(MeanDir_1_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_2_All{s}, Mu2_est, Pm);
                    else
                        [cs1, Mu1_est] = SymDistance(MeanDir_2_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_1_All{s}, Mu2_est, Pm);
                    end
                case 'VMF'
                    %%% Estimate by EM
                    [Mu_est, Kappa_est, W_est]=VMFEM_Hyper(Samples, Pm, 2);
                    Mu1_est = Mu_est(1,:);
                    Mu2_est = Mu_est(2,:);
                    Kappa1_est = Kappa_est(1);
                    Kappa2_est = Kappa_est(2);
                    if(W_est(1)<0.5)
                        [cs1, Mu1_est] = SymDistance(MeanDir_1_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_2_All{s}, Mu2_est, Pm);
                    else
                        [cs1, Mu1_est] = SymDistance(MeanDir_2_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_1_All{s}, Mu2_est, Pm);
                    end
                case 'Watson'
                    %%% Estimate by Watson EM
                    [Mu_est, Kappa_est, W_est]=WatsonEM(Samples, Pm, 2);
                    Mu1_est = Mu_est(1,:);
                    Mu2_est = Mu_est(2,:);
                    Kappa1_est = Kappa_est(1);
                    Kappa2_est = Kappa_est(2);
                    if(W_est(1)<0.5)
                        [cs1, Mu1_est] = SymDistance(MeanDir_1_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_2_All{s}, Mu2_est, Pm);
                    else
                        [cs1, Mu1_est] = SymDistance(MeanDir_2_All{s}, Mu1_est, Pm);
                        [cs2, Mu2_est] = SymDistance(MeanDir_1_All{s}, Mu2_est, Pm);
                    end
            end
            Cosin_All(s) = (cs1+cs2)/2;
            Kappa_All(s,:) = [Kappa1_est, Kappa2_est];
        end
        save(result_file,'Cosin_All', 'Kappa_All');
    end
end
