% The overall simulation process. Related the Figure 1, 2 in the paper.

%% Data generation
GenerateParsEstimationData;

%% Parameter Estimation by EMVMF
CompareParameterEstimation;

%% Generate the figures
SimResultParsEstimationFigures;
