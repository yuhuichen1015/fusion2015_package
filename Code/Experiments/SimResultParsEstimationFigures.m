%% Show the VMF simulated data result.
Data = 'VMF';
%%% Show Result
KappaSet = [1:100];
Num_of_Kappa = size(KappaSet(:),1);

% Specify the methods to be displayed and their line specs and legneds.
Methods = {'Ori', 'Cls', 'VMF', 'Watson', 'VMF_Hyper'};
Line_specs = {'b-','g--','k.','m-.', 'c-o'};
Legends = {'ML Estimator', 'Modified ML', 'EM-VMF', 'EM-Watson', 'EM-VMF_{Hyper}'};

Cosin_Mean_All = cell(size(Methods(:),1),1);
Kappa_Mean_All = cell(size(Methods(:),1),1);
Time_Mean_All = cell(size(Methods(:),1),1);

for mtd = 1:size(Methods(:),1)
    Cosin_Mean = zeros(size(KappaSet(:)));
    Kappa_Mean = zeros(size(KappaSet(:)));
    Time_Mean = zeros(size(KappaSet(:)));
    method = Methods{mtd};
    for i=1:size(KappaSet(:),1)
        kappa = KappaSet(i);
        %%% Testing EM algorithm
        result_file = [result_path 'ParsEstimation/' Data '_FZ_K' int2str(kappa) '_' method '_Result.mat'];
        load(result_file);
        
        Cosin_Mean(i) = mean(Cosin_All(:));
        Kappa_Mean(i) = mean(Kappa_All(:));
        Time_Mean(i) = mean(Time_All(:));
    end
    Cosin_Mean_All{mtd} = Cosin_Mean;
    Kappa_Mean_All{mtd} = Kappa_Mean;
    Time_Mean_All{mtd} = Time_Mean;
end

set(0, 'DefaultAxesLineWidth', 2.0)
set(0, 'DefaultLineLineWidth', 2.0)
set(0, 'DefaultTextFontSize', 15)
set(0, 'DefaultTextFontWeight', 'bold')
set(0, 'DefaultAxesFontSize', 15)
set(0, 'DefaultAxesFontWeight', 'bold')
set(0, 'DefaultLineMarkerSize', 10)

%%% Mean direction estimation
x=KappaSet;
figure;
% Plot the ground truth
H = plot(KappaSet, ones(Num_of_Kappa,1), 'r');
M = [{'Ground Truth'} Legends];
hold on;
for mtd = 1:size(Methods(:),1)
    h=plot(KappaSet, Cosin_Mean_All{mtd}, Line_specs{mtd}),...
        xlabel('$$\kappa_o$$', 'interpreter', 'latex'),...
        ylabel('$$\mu^T\mu_o$$', 'interpreter', 'latex'),...
        axis([0 max(KappaSet(:)) 0.92 1.01]);
    H = [H; h];
end
legend(H,M);

%%% Kappa estimation
figure;
% Plot the ground truth
H = plot(KappaSet, KappaSet, 'r');
M = [{'Ground Truth'} Legends];
hold on;
for mtd = 1:size(Methods(:),1)
    h = plot(KappaSet, Kappa_Mean_All{mtd}, Line_specs{mtd});...
        xlabel('$$\kappa_o$$', 'interpreter', 'latex'),...
        ylabel('$$\mu^T\mu_o$$', 'interpreter', 'latex');
    H = [H; h];
end
legend(H,M);

%%% Computation Time
figure; hold on;
% Plot the ground truth
H = [];
M = Legends;
for mtd = 1:size(Methods(:),1)
    h=plot(KappaSet, Time_Mean_All{mtd}, Line_specs{mtd}),...
        xlabel('$$\kappa_o$$', 'interpreter', 'latex'),...
        ylabel('Computation Time', 'interpreter', 'latex');
    H = [H; h];
end
legend(H,M);

%% Show the Watson simulated data result.
Data = 'Watson';
%%% Show Result
KappaSet = [1:100];
Num_of_Kappa = size(KappaSet(:),1);

% Specify the methods to be displayed and their line specs and legneds.
Methods = {'Ori', 'Cls', 'VMF', 'Watson', 'VMF_Hyper'};
Line_specs = {'b-','g--','k.','m-.', 'c-o'};
Legends = {'ML Estimator', 'Modified ML', 'EM-VMF', 'EM-Watson', 'EM-VMF_{Hyper}'};

Cosin_Mean_All = cell(size(Methods(:),1),1);
Kappa_Mean_All = cell(size(Methods(:),1),1);
Time_Mean_All = cell(size(Methods(:),1),1);

for mtd = 1:size(Methods(:),1)
    Cosin_Mean = zeros(size(KappaSet(:)));
    Kappa_Mean = zeros(size(KappaSet(:)));
    Time_Mean = zeros(size(KappaSet(:)));
    method = Methods{mtd};
    for i=1:size(KappaSet(:),1)
        kappa = KappaSet(i);
        %%% Testing EM algorithm
        result_file = [result_path 'ParsEstimation/' Data '_FZ_K' int2str(kappa) '_' method '_Result.mat'];
        load(result_file);
        
        Cosin_Mean(i) = mean(Cosin_All(:));
        Kappa_Mean(i) = mean(Kappa_All(:));
        Time_Mean(i) = mean(Time_All(:));
    end
    Cosin_Mean_All{mtd} = Cosin_Mean;
    Kappa_Mean_All{mtd} = Kappa_Mean;
    Time_Mean_All{mtd} = Time_Mean;
end

set(0, 'DefaultAxesLineWidth', 2.0)
set(0, 'DefaultLineLineWidth', 2.0)
set(0, 'DefaultTextFontSize', 15)
set(0, 'DefaultTextFontWeight', 'bold')
set(0, 'DefaultAxesFontSize', 15)
set(0, 'DefaultAxesFontWeight', 'bold')
set(0, 'DefaultLineMarkerSize', 10)

%%% Mean direction estimation
x=KappaSet;
figure;
% Plot the ground truth
H = plot(KappaSet, ones(Num_of_Kappa,1), 'r');
M = [{'Ground Truth'} Legends];
hold on;
for mtd = 1:size(Methods(:),1)
    h=plot(KappaSet, Cosin_Mean_All{mtd}, Line_specs{mtd}),...
        xlabel('$$\kappa_o$$', 'interpreter', 'latex'),...
        ylabel('$$\mu^T\mu_o$$', 'interpreter', 'latex'),...
        axis([0 max(KappaSet(:)) 0.92 1.01]);
    H = [H; h];
end
legend(H,M);

%%% Kappa estimation
figure;
% Plot the ground truth
H = plot(KappaSet, KappaSet, 'r');
M = [{'Ground Truth'} Legends];
hold on;
for mtd = 1:size(Methods(:),1)
    h = plot(KappaSet, Kappa_Mean_All{mtd}, Line_specs{mtd});...
        xlabel('$$\kappa_o$$', 'interpreter', 'latex'),...
        ylabel('$$\mu^T\mu_o$$', 'interpreter', 'latex');
    H = [H; h];
end
legend(H,M);

%%% Computation Time
figure; hold on;
% Plot the ground truth
H = [];
M = Legends;
for mtd = 1:size(Methods(:),1)
    h=plot(KappaSet, Time_Mean_All{mtd}, Line_specs{mtd}),...
        xlabel('$$\kappa_o$$', 'interpreter', 'latex'),...
        ylabel('Computation Time', 'interpreter', 'latex');
    H = [H; h];
end
legend(H,M);