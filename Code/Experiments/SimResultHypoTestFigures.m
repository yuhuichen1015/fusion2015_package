%% Whow VMF simultaed result
Data = 'VMF';
KappaSet = [50];
Num_of_Kappa = size(KappaSet(:),1);
% Specify the methods to be displayed and their line specs and legneds.
Methods = {'Ori', 'Cls', 'VMF', 'Watson'};
Line_specs = {'b-','g--','k-','m-.'};
Legends = {'ML Estimator', 'Modified ML', 'EM-VMF', 'EM-Watson'};

for kappa=KappaSet
    ROCCurves = {};
    for mtd = 1:size(Methods(:),1)
        method = Methods{mtd};
        %%% Testing EM algorithm
        result_file = [result_path 'HypoTest/' Data '_FZ_2C_K' int2str(kappa) '_' method '_Result.mat'];
        load(result_file);
        
        data_file = [data_path 'HypoTest/' Data '_FZ_2C_K' int2str(kappa) '.mat'];
        load(data_file);
        
        Num_of_bins = 200;
        Threshold = linspace(min(logGLRT_All(:)), max(logGLRT_All(:)), Num_of_bins);
        FPR_All = zeros(Num_of_bins,1);
        TPR_All = zeros(Num_of_bins,1);
        for th = 1:Num_of_bins
            thresh = Threshold(th);
            P = sum(Label_All(:)==1);
            N = sum(Label_All(:)==0);
            TP = sum(logGLRT_All(:)>thresh & Label_All(:)==1);
            FP = sum(logGLRT_All(:)>thresh & Label_All(:)==0);
            TPR_All(th) = TP/P;
            FPR_All(th) = FP/N;
        end
        ROCCurves = [ROCCurves; {FPR_All, TPR_All}];
    end
    
    set(0, 'DefaultAxesLineWidth', 2.0)
    set(0, 'DefaultLineLineWidth', 2.0)
    set(0, 'DefaultTextFontSize', 15)
    set(0, 'DefaultTextFontWeight', 'bold')
    set(0, 'DefaultAxesFontSize', 15)
    set(0, 'DefaultAxesFontWeight', 'bold')
    set(0, 'DefaultLineMarkerSize', 10)
    
    %%% Computation Time
    figure; hold on;
    % Plot the ground truth
    H = [];
    M = Legends;
    for mtd = 1:size(Methods(:),1)
        h=plot(ROCCurves{mtd,1}, ROCCurves{mtd,2}, Line_specs{mtd}),...
            xlabel('False Positive Rate', 'interpreter', 'latex'),...
            ylabel('True Positive Rate', 'interpreter', 'latex'),...
            title(['\kappa = ' int2str(kappa)]);
        
        H = [H; h];
    end
    legend(H,M);
end


%% Show Watson simulated result
Data = 'Watson';
KappaSet = [50];
Num_of_Kappa = size(KappaSet(:),1);
% Specify the methods to be displayed and their line specs and legneds.
Methods = {'Ori', 'Cls', 'VMF', 'Watson'};
Line_specs = {'b-','g--','k-','m-.'};
Legends = {'ML Estimator', 'Modified ML', 'EM-VMF', 'EM-Watson'};

for kappa=KappaSet
    ROCCurves = {};
    for mtd = 1:size(Methods(:),1)
        method = Methods{mtd};
        %%% Testing EM algorithm
        result_file = [result_path 'HypoTest/' Data '_FZ_2C_K' int2str(kappa) '_' method '_Result.mat'];
        load(result_file);
        
        data_file = [data_path 'HypoTest/' Data '_FZ_2C_K' int2str(kappa) '.mat'];
        load(data_file);
        
        Num_of_bins = 200;
        Threshold = linspace(min(logGLRT_All(:)), max(logGLRT_All(:)), Num_of_bins);
        FPR_All = zeros(Num_of_bins,1);
        TPR_All = zeros(Num_of_bins,1);
        for th = 1:Num_of_bins
            thresh = Threshold(th);
            P = sum(Label_All(:)==1);
            N = sum(Label_All(:)==0);
            TP = sum(logGLRT_All(:)>thresh & Label_All(:)==1);
            FP = sum(logGLRT_All(:)>thresh & Label_All(:)==0);
            TPR_All(th) = TP/P;
            FPR_All(th) = FP/N;
        end
        ROCCurves = [ROCCurves; {FPR_All, TPR_All}];
    end
    
    set(0, 'DefaultAxesLineWidth', 2.0)
    set(0, 'DefaultLineLineWidth', 2.0)
    set(0, 'DefaultTextFontSize', 15)
    set(0, 'DefaultTextFontWeight', 'bold')
    set(0, 'DefaultAxesFontSize', 15)
    set(0, 'DefaultAxesFontWeight', 'bold')
    set(0, 'DefaultLineMarkerSize', 10)
    
    %%% Computation Time
    figure; hold on;
    % Plot the ground truth
    H = [];
    M = Legends;
    for mtd = 1:size(Methods(:),1)
        h=plot(ROCCurves{mtd,1}, ROCCurves{mtd,2}, Line_specs{mtd}),...
            xlabel('False Positive Rate', 'interpreter', 'latex'),...
            ylabel('True Positive Rate', 'interpreter', 'latex'),...
            title(['\kappa = ' int2str(kappa)]);
        
        H = [H; h];
    end
    legend(H,M);
end




