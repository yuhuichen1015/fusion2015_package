%% Show the VMF simulated data result.
Data = 'VMF';
KappaSet = [1:100];
Num_of_Kappa = size(KappaSet(:),1);

Methods = {'Ori', 'Cls', 'VMF', 'Watson'};
Line_specs = {'b-','g--','c-o','m-.'};
Legends = {'K-means', 'Modified K-means', 'EM-VMF', 'EM-Watson'};

Cosin_Mean_All = cell(size(Methods(:),1),1);
for mtd = 1:size(Methods(:),1)
    Cosin_Mean = zeros(size(KappaSet(:),1),1);
    method = Methods{mtd};
    for i=1:size(KappaSet(:),1)
        kappa = KappaSet(i);
        result_file = [result_path 'Clustering/' Data '_FZ_2C_K' int2str(kappa) '_' method '_Result.mat'];
        load(result_file);
        
        Cosin_Mean(i) = mean(Cosin_All);
    end
    Cosin_Mean_All{mtd} = Cosin_Mean;
end
    
set(0, 'DefaultAxesLineWidth', 2.0)
set(0, 'DefaultLineLineWidth', 2.0)
set(0, 'DefaultTextFontSize', 15)
set(0, 'DefaultTextFontWeight', 'bold')
set(0, 'DefaultAxesFontSize', 15)
set(0, 'DefaultAxesFontWeight', 'bold')
set(0, 'DefaultLineMarkerSize', 10)

x=KappaSet;
figure;
% Plot the ground truth
H = plot(KappaSet, ones(Num_of_Kappa,1), 'r');
M = [{'Ground Truth'} Legends];
hold on;
for mtd = 1:size(Methods(:),1)
    h=plot(KappaSet, Cosin_Mean_All{mtd}, Line_specs{mtd}),...
        xlabel('$$\kappa_o$$', 'interpreter', 'latex'),...
        ylabel('$$\mu^T\mu_o$$', 'interpreter', 'latex'),...
        axis([0 max(KappaSet(:)) 0.92 1.01]);
    H = [H; h];
end
legend(H,M);


%% Show the Watson simulated data result.
Data = 'Watson';
KappaSet = [1:100];
Num_of_Kappa = size(KappaSet(:),1);

Methods = {'Ori', 'Cls', 'VMF', 'Watson'};
Line_specs = {'b-','g--','c-o','m-.'};
Legends = {'K-means', 'Modified K-means', 'EM-VMF', 'EM-Watson'};

Cosin_Mean_All = cell(size(Methods(:),1),1);
for mtd = 1:size(Methods(:),1)
    Cosin_Mean = zeros(size(KappaSet(:),1),1);
    method = Methods{mtd};
    for i=1:size(KappaSet(:),1)
        kappa = KappaSet(i);
        result_file = [result_path 'Clustering/' Data '_FZ_2C_K' int2str(kappa) '_' method '_Result.mat'];
        load(result_file);
        
        Cosin_Mean(i) = mean(Cosin_All);
    end
    Cosin_Mean_All{mtd} = Cosin_Mean;
end
    
set(0, 'DefaultAxesLineWidth', 2.0)
set(0, 'DefaultLineLineWidth', 2.0)
set(0, 'DefaultTextFontSize', 15)
set(0, 'DefaultTextFontWeight', 'bold')
set(0, 'DefaultAxesFontSize', 15)
set(0, 'DefaultAxesFontWeight', 'bold')
set(0, 'DefaultLineMarkerSize', 10)

x=KappaSet;
figure;
% Plot the ground truth
H = plot(KappaSet, ones(Num_of_Kappa,1), 'r');
M = [{'Ground Truth'} Legends];
hold on;
for mtd = 1:size(Methods(:),1)
    h=plot(KappaSet, Cosin_Mean_All{mtd}, Line_specs{mtd}),...
        xlabel('$$\kappa_o$$', 'interpreter', 'latex'),...
        ylabel('$$\mu^T\mu_o$$', 'interpreter', 'latex'),...
        axis([0 max(KappaSet(:)) 0.92 1.01]);
    H = [H; h];
end
legend(H,M);
