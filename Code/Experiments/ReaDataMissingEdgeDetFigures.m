
Alpha = 0.05;
Sigma = 0.5;

load([data_path 'IndexingResult_IN100.mat']);
load([data_path 'BSE_IN100_reg.mat']);

EA1 = SelectRegion(EA1, Selected_pts, Height, Width);
EA2 = SelectRegion(EA2, Selected_pts, Height, Width);
EA3 = SelectRegion(EA3, Selected_pts, Height, Width);
I_BSE = SelectRegion(I_BSE, Selected_pts, Height, Width);
Indexing = SelectRegion(Indexing, Selected_pts, Height, Width);
PatternQuality = SelectRegion(PatternQuality, Selected_pts, Height, Width);
Boundary = imgIdx2Boundary(Indexing);
Indexing(Boundary==1) = 0;
inds = unique(Indexing(:));
inds(1) = [];
figure; imagesc(Indexing);
% OEM IPF coloring image
Phi = EA1;
Phi = cat(3, Phi, EA2);
Phi = cat(3, Phi, EA3);
[Img] = imgPhi2Img(Phi, (~Boundary & PatternQuality==1), 3, 1);
figure; image(Img);

%%%
Methods = {'VMF', 'Watson'};
for mtd = 1:size(Methods(:),1)
    method = Methods{mtd};
    load([result_path 'Hypotest_Comparisons_IN100_' method '.mat']);
    % Create mu image
    logGLRT_Img = zeros(Height,Width);
    eta = chi2inv(1-Alpha, 5);
    DetectedInd = [];
    DetectPhi = zeros(numel(Indexing),3);
    DetectKappa = zeros(size(Indexing));
    for g=1:size(inds(:),1)
        logGLRT_Img(Indexing==inds(g)) = H1_All(inds(g))-H0_All(inds(g));
        n1 = sum((Indexing(:)==inds(g) & Indexing_Split(:)==1));
        n2 = sum((Indexing(:)==inds(g) & Indexing_Split(:)==2));
        n = sum(Indexing(:)==inds(g));
        if(2*(H1_All(inds(g))-H0_All(inds(g))) > eta)
            calcThresh(n, Sigma, Alpha);
            DetectedInd = [DetectedInd; inds(g)];
            badpixels = Indexing(:)==inds(g) & PatternQuality(:)==0;
            if(n1<n2)
                mu_est = Mu_All{inds(g)};
                kappa_est = Kappa_All(inds(g),:);
                [p, issig] = SpinCalc('QtoEA313', mu_est(1,:), 0.01, 0);
                region = Indexing(:)==inds(g) & Indexing_Split(:)==1;
                DetectPhi(region(:),:) = repmat(p*pi/180, [sum(region(:)), 1]);
                DetectKappa(region(:)) = kappa_est(1);
                [p, issig] = SpinCalc('QtoEA313', mu_est(2,:), 0.01, 0);
                region = (Indexing(:)==inds(g) & Indexing_Split(:)==2) | badpixels;
                DetectPhi(region(:),:) = repmat(p*pi/180, [sum(region(:)), 1]);
                DetectKappa(region(:)) = kappa_est(2);
            else
                mu_est = Mu_All{inds(g)};
                kappa_est = Kappa_All(inds(g),:);
                [p, issig] = SpinCalc('QtoEA313', mu_est(1,:), 0.01, 0);
                region = (Indexing(:)==inds(g) & Indexing_Split(:)==1) | badpixels;
                DetectPhi(region(:),:) = repmat(p*pi/180, [sum(region(:)), 1]);
                DetectKappa(region(:)) = kappa_est(1);
                [p, issig] = SpinCalc('QtoEA313', mu_est(2,:), 0.01, 0);
                region = Indexing(:)==inds(g) & Indexing_Split(:)==2;
                DetectPhi(region(:),:) = repmat(p*pi/180, [sum(region(:)), 1]);
                DetectKappa(region(:)) = kappa_est(2);
            end
        else
            [p, issig] = SpinCalc('QtoEA313', Mu0_All{inds(g)}, 0.01, 0);
            DetectPhi(Indexing(:)==inds(g),:) = repmat(p*pi/180, [sum(Indexing(:)==inds(g)), 1]);
            DetectKappa(Indexing(:)==inds(g)) = Kappa0_All(inds(g));
        end
    end
    DetectPhi = reshape(DetectPhi, [size(Indexing,1), size(Indexing,2), 3]);
    [DetectImg] = imgPhi2Img(DetectPhi, ~Boundary, 3, 1);
    fig=figure; image(DetectImg), axis image, title('Estimated Mean Direction');
    %%% Draw the newly detected boundary
    se = strel('disk', 2);
    se_s = strel('disk', 1);
    for g=1:size(DetectedInd(:))
        region = Indexing==DetectedInd(g);
        n1 = sum(region(:) & Indexing_Split(:)==1);
        n2 = sum(region(:) & Indexing_Split(:)==2);
        if(n1<n2)
            IndexingNewGrain = (region & Indexing_Split==1);
        else
            IndexingNewGrain = (region & Indexing_Split==2);
        end
        BoundaryNewGrain = boolean(imdilate(IndexingNewGrain, se) - IndexingNewGrain)...
            & region;
        BoundaryNewGrain_thin = boolean(imdilate(IndexingNewGrain, se_s) - IndexingNewGrain)...
            & region;
        tmpImg = DetectImg(:,:,1);
        tmpImg(BoundaryNewGrain) = 1;
        DetectImg(:,:,1) = tmpImg;
        tmpImg = DetectImg(:,:,2);
        tmpImg(BoundaryNewGrain) = 1;
        DetectImg(:,:,2) = tmpImg;
        tmpImg = DetectImg(:,:,3);
        tmpImg(BoundaryNewGrain) = 1;
        DetectImg(:,:,3) = tmpImg;
        DetectKappa(BoundaryNewGrain_thin) = 0;
    end
    fig=figure;
    set(fig, 'DefaultAxesLineWidth', 2.0)
    set(fig, 'DefaultLineLineWidth', 2.0)
    set(fig, 'DefaultTextFontSize', 15)
    set(fig, 'DefaultTextFontWeight', 'bold')
    set(fig, 'DefaultAxesFontSize', 18)
    set(fig, 'DefaultAxesFontWeight', 'bold')
    set(fig, 'DefaultLineMarkerSize', 10)
    set(fig, 'PaperPositionMode', 'auto');
    image(DetectImg), axis image, title('Detected Grains');

    fig=figure; 
    set(fig, 'DefaultAxesLineWidth', 2.0)
    set(fig, 'DefaultLineLineWidth', 2.0)
    set(fig, 'DefaultTextFontSize', 15)
    set(fig, 'DefaultTextFontWeight', 'bold')
    set(fig, 'DefaultAxesFontSize', 18)
    set(fig, 'DefaultAxesFontWeight', 'bold')
    set(fig, 'DefaultLineMarkerSize', 10)
    set(fig, 'PaperPositionMode', 'auto');
    imagesc(DetectKappa), axis image, colormap gray, colorbar, title('Estimated \kappa');
end
%%%

