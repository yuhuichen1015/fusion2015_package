% The overall simulation process. Related the Figure 5 in the paper.

%% Data generation
GenerateHypoTestData;

%% Parameter Estimation by EMVMF
CompareHypoTest;

%% Generate the figures
SimResultHypoTestFigures;
