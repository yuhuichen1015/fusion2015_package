Methods = {'Ori', 'Cls', 'VMF', 'Watson'};
mkdir([result_path 'HypoTest/']);
SymOperatorsSrp;


%% Compare the result by samples generated from VMF.
for Kappa=KappaSet
    ['Computing the result for kappa = ' int2str(Kappa)]
    %%% Testing EM algorithm
    input_file = [data_path 'HypoTest/VMF_FZ_2C_K' int2str(Kappa) '.mat'];
    load(input_file);
    
    for mtd = 1:size(Methods(:),1)
        method = Methods{mtd};
        result_file = [result_path 'HypoTest/VMF_FZ_2C_K' int2str(Kappa) '_' method '_Result.mat'];
        if(exist(result_file, 'file') && ~overwrite_computation)
            warning(['Result for kappa = ' int2str(Kappa) ' exists. Computation skipped']);
            continue;
        end
        
        Num_of_sets = size(MeanDir_1_All,1);
        H0_All = zeros(Num_of_sets,1);   
        H1_All = zeros(Num_of_sets,1);
        for s=1:Num_of_sets
            Samples = Samples_All{s};
            switch(method)
                case 'Ori' % Origin ML estimator with kmeans
%                     method
                    [idx,ctrs] = mykmeans(Samples,2,'Distance','cosine','Replicates',5);
                    X1 = Samples(idx==1,:);
                    X2 = Samples(idx==2,:);
                    [Mu1_est, Kappa1_est] = OriginEstforVMF(X1);
                    [Mu2_est, Kappa2_est] = OriginEstforVMF(X2);
                    [Mu0_est, Kappa0_est] = OriginEstforVMF(Samples);
                    H0_All(s) = sum(log(VMFDensity(Samples, Mu0_est, Kappa0_est)));
                    H1_All(s) = sum(log(VMFDensity(X1,Mu1_est,Kappa1_est))) + sum(log(VMFDensity(X2,Mu2_est,Kappa2_est)));
                case 'Cls'
%                     method
                    [idx,ctrs] = mykmeans(Samples,2,'Distance','SymMetric','Replicates',5);
                    X1 = Samples(idx==1,:);
                    X2 = Samples(idx==2,:);
                    [Mu1_est, Kappa1_est, X1] = CloestEstforVMF(X1, Pm);
                    [Mu2_est, Kappa2_est, X2] = CloestEstforVMF(X2, Pm);
                    [Mu0_est, Kappa0_est, X0] = CloestEstforVMF(Samples, Pm);
                    H0_All(s) = sum(log(VMFDensity(X0, Mu0_est, Kappa0_est)));
                    H1_All(s) = sum(log(VMFDensity(X1,Mu1_est,Kappa1_est))) + sum(log(VMFDensity(X2,Mu2_est,Kappa2_est)));
                case 'VMF'
%                   method
                    %%% Estimate by EM
                    [Mu_est, Kappa_est, W_est, logL0]=VMFEM_Hyper(Samples, Pm, 1);
                    [Mu_est, Kappa_est, W_est, logL1]=VMFEM_Hyper(Samples, Pm, 2);
                    H0_All(s) = logL0;
                    H1_All(s) = logL1;
                
                case 'Watson'
%                     method
                    %%% Estimate by Watson EM
                    [Mu_est, Kappa_est, W_est, logL0]=WatsonEM(Samples, Pm, 1);
                    [Mu_est, Kappa_est, W_est, logL1]=WatsonEM(Samples, Pm, 2);
                    H0_All(s) = logL0;
                    H1_All(s) = logL1;
            end
        end
        logGLRT_All = H1_All-H0_All;
        save(result_file,'logGLRT_All', 'H0_All', 'H1_All');
    end
end

%% Compare the result by samples generated from Watson.
for Kappa=KappaSet
    ['Computing the result for kappa = ' int2str(Kappa)]
    %%% Testing EM algorithm
    input_file = [data_path 'HypoTest/Watson_FZ_2C_K' int2str(Kappa) '.mat'];
    load(input_file);
    
    for mtd = 1:size(Methods(:),1)
        method = Methods{mtd};
        result_file = [result_path 'HypoTest/Watson_FZ_2C_K' int2str(Kappa) '_' method '_Result.mat'];
        if(exist(result_file, 'file') && ~overwrite_computation)
            warning(['Result for kappa = ' int2str(Kappa) ' exists. Computation skipped']);
            continue;
        end
        
        Num_of_sets = size(MeanDir_1_All,1);
        H0_All = zeros(Num_of_sets,1);   
        H1_All = zeros(Num_of_sets,1);
        for s=1:Num_of_sets
            Samples = Samples_All{s};
            switch(method)
                case 'Ori' % Origin ML estimator with kmeans
                    [idx,ctrs] = mykmeans(Samples,2,'Distance','cosine','Replicates',5);
                    X1 = Samples(idx==1,:);
                    X2 = Samples(idx==2,:);
                    [Mu1_est, Kappa1_est] = OriginEstforVMF(X1);
                    [Mu2_est, Kappa2_est] = OriginEstforVMF(X2);
                    [Mu0_est, Kappa0_est] = OriginEstforVMF(Samples);
                    H0_All(s) = sum(log(VMFDensity(Samples, Mu0_est, Kappa0_est)));
                    H1_All(s) = sum(log(VMFDensity(X1,Mu1_est,Kappa1_est))) + sum(log(VMFDensity(X2,Mu2_est,Kappa2_est)));
                case 'Cls'
                    [idx,ctrs] = mykmeans(Samples,2,'Distance','SymMetric','Replicates',5);
                    X1 = Samples(idx==1,:);
                    X2 = Samples(idx==2,:);
                    [Mu1_est, Kappa1_est, X1] = CloestEstforVMF(X1, Pm);
                    [Mu2_est, Kappa2_est, X2] = CloestEstforVMF(X2, Pm);
                    [Mu0_est, Kappa0_est, X0] = CloestEstforVMF(Samples, Pm);
                    H0_All(s) = sum(log(VMFDensity(X0, Mu0_est, Kappa0_est)));
                    H1_All(s) = sum(log(VMFDensity(X1,Mu1_est,Kappa1_est))) + sum(log(VMFDensity(X2,Mu2_est,Kappa2_est)));
                case 'VMF'
                    %%% Estimate by EM
                    [Mu_est, Kappa_est, W_est, logL0]=VMFEM_Hyper(Samples, Pm, 1);
                    [Mu_est, Kappa_est, W_est, logL1]=VMFEM_Hyper(Samples, Pm, 2);
                    H0_All(s) = logL0;
                    H1_All(s) = logL1;
                
                case 'Watson'
                    %%% Estimate by Watson EM
                    [Mu_est, Kappa_est, W_est, logL0]=WatsonEM(Samples, Pm, 1);
                    [Mu_est, Kappa_est, W_est, logL1]=WatsonEM(Samples, Pm, 2);
                    H0_All(s) = logL0;
                    H1_All(s) = logL1;
            end
        end
        logGLRT_All = H1_All-H0_All;
        save(result_file,'logGLRT_All', 'H0_All', 'H1_All');
    end
end