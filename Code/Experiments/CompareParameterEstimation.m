% Copmare the parameter estimation performance for different methods:
% 1. Maximum likelihood estimator
% 2. Modified ML estimator
% 3. EM for VMF estimator
% 4. EM for Watson estimator
% 5. EM for VMF Hyper estimator

Methods = {'Ori', 'Cls', 'VMF', 'VMF_Hyper', 'Watson'};
mkdir([result_path 'ParsEstimation/']);
SymOperatorsSrp;

%% Compare the result by samples generated from VMF.
for Kappa=KappaSet
    ['Computing the result for kappa = ' int2str(Kappa)]
    %%% Testing EM algorithm
    input_file = [data_path 'ParsEstimation/VMF_FZ_K' int2str(Kappa) '.mat'];
    load(input_file);

    for mtd = 1:size(Methods(:),1)
        method = Methods{mtd};
        result_file = [result_path 'ParsEstimation/VMF_FZ_K' int2str(Kappa) '_' method '_Result.mat'];
        if(exist(result_file, 'file') && ~overwrite_computation)
            warning(['Result for kappa = ' int2str(Kappa) ' exists. Computation skipped']);
            continue;
        end
 
        Cosin_All = zeros(Num_of_sets,1);
        Time_All = zeros(Num_of_sets,1);
        Kappa_All = zeros(Num_of_sets,1);
        for s=1:Num_of_sets
            Samples = Samples_All{s};
            switch(method)
                case 'Ori'
                    tic;
                    [Mu_est, Kappa_est] = OriginEstforVMF(Samples);
                    time = toc;
                    Time_All(s) = time;
                    [cs, Mu_est] = SymDistance(MeanDir_All{s}, Mu_est, Pm);
                    Cosin_All(s) = cs;
                    Kappa_All(s) = Kappa_est;
                case 'Cls'
                    tic;
                    [Mu_est, Kappa_est] = CloestEstforVMF(Samples, Pm);
                    time = toc;
                    Time_All(s) = time;
                    [cs, Mu_est] = SymDistance(MeanDir_All{s}, Mu_est, Pm);
                    Cosin_All(s) = cs;
                    Kappa_All(s) = Kappa_est;
                case 'VMF'
                    tic;
                    [Mu_est, Kappa_est]=VMFEM(Samples, Pm);
                    time = toc;
                    Time_All(s) = time;
                    [cs, Mu_est] = SymDistance(MeanDir_All{s}, Mu_est, Pm);
                    Cosin_All(s) = cs;
                    Kappa_All(s) = Kappa_est;
                case 'VMF_Hyper'
                    tic;
                    [Mu_est, Kappa_est]=VMFEM_Hyper(Samples, Pm);
                    time = toc;
                    Time_All(s) = time;
                    [cs, Mu_est] = SymDistance(MeanDir_All{s}, Mu_est, Pm);
                    Cosin_All(s) = cs;
                    Kappa_All(s) = Kappa_est;
                case 'Watson'
                    tic;
                    [Mu_est, Kappa_est]=WatsonEM(Samples, Pm);
                    time = toc;
                    Time_All(s) = time;
                    [cs, Mu_est] = SymDistance(MeanDir_All{s}, Mu_est, Pm);
                    Cosin_All(s) = cs;
                    Kappa_All(s) = Kappa_est;
            end
        end
        save(result_file, 'Cosin_All', 'Kappa_All', 'Time_All');
    end
end

%% Compare the result by samples generated from Watson.
for Kappa=KappaSet
    ['Computing the result for kappa = ' int2str(Kappa)]
    %%% Testing EM algorithm
    input_file = [data_path 'ParsEstimation/Watson_FZ_K' int2str(Kappa) '.mat'];
    load(input_file);

    for mtd = 1:size(Methods(:),1)
        method = Methods{mtd};
        result_file = [result_path 'ParsEstimation/Watson_FZ_K' int2str(Kappa) '_' method '_Result.mat'];
        if(exist(result_file, 'file') && ~overwrite_computation)
            warning(['Result for kappa = ' int2str(Kappa) ' exists. Computation skipped']);
            continue;
        end
 
        Cosin_All = zeros(Num_of_sets,1);
        Time_All = zeros(Num_of_sets,1);
        Kappa_All = zeros(Num_of_sets,1);
        for s=1:Num_of_sets
            Samples = Samples_All{s};
            switch(method)
                case 'Ori'
                    tic;
                    [Mu_est, Kappa_est] = OriginEstforVMF(Samples);
                    time = toc;
                    Time_All(s) = time;
                    [cs, Mu_est] = SymDistance(MeanDir_All{s}, Mu_est, Pm);
                    Cosin_All(s) = cs;
                    Kappa_All(s) = Kappa_est;
                case 'Cls'
                    tic;
                    [Mu_est, Kappa_est] = CloestEstforVMF(Samples, Pm);
                    time = toc;
                    Time_All(s) = time;
                    [cs, Mu_est] = SymDistance(MeanDir_All{s}, Mu_est, Pm);
                    Cosin_All(s) = cs;
                    Kappa_All(s) = Kappa_est;
                case 'VMF'
                    tic;
                    [Mu_est, Kappa_est]=VMFEM(Samples, Pm);
                    time = toc;
                    Time_All(s) = time;
                    [cs, Mu_est] = SymDistance(MeanDir_All{s}, Mu_est, Pm);
                    Cosin_All(s) = cs;
                    Kappa_All(s) = Kappa_est;
                case 'VMF_Hyper'
                    tic;
                    [Mu_est, Kappa_est]=VMFEM_Hyper(Samples, Pm);
                    time = toc;
                    Time_All(s) = time;
                    [cs, Mu_est] = SymDistance(MeanDir_All{s}, Mu_est, Pm);
                    Cosin_All(s) = cs;
                    Kappa_All(s) = Kappa_est;
                case 'Watson'
                    tic;
                    [Mu_est, Kappa_est]=WatsonEM(Samples, Pm);
                    time = toc;
                    Time_All(s) = time;
                    [cs, Mu_est] = SymDistance(MeanDir_All{s}, Mu_est, Pm);
                    Cosin_All(s) = cs;
                    Kappa_All(s) = Kappa_est;
            end
        end
        save(result_file, 'Cosin_All', 'Kappa_All', 'Time_All');
    end
end

