% The real data experiment process. Related the Figure 3 in the paper.

%% Parameter Estimation for real data.
GenerateRealDataMissingEdgeDet;

%% Generate the figures
ReaDataMissingEdgeDetFigures;
