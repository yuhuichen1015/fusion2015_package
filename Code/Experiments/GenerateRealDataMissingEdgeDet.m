
load([data_path 'IndexingResult_IN100.mat']);
SymOperatorsSrp;
Boundary = imgIdx2Boundary(Indexing);

%%% Select region
EA1 = SelectRegion(EA1, Selected_pts, Height, Width);
EA2 = SelectRegion(EA2, Selected_pts, Height, Width);
EA3 = SelectRegion(EA3, Selected_pts, Height, Width);
Indexing = SelectRegion(Indexing, Selected_pts, Height, Width);
PatternQuality = SelectRegion(PatternQuality, Selected_pts, Height, Width);
Boundary = SelectRegion(Boundary, Selected_pts, Height, Width);

% Specify the methods to be displayed and their line specs and legneds.
Methods = {'VMF','Watson'};

Num_of_grains = max(Indexing(:));
GrainID = unique(Indexing(:));

for mtd = 1:size(Methods(:),1)
    result_file = [result_path 'Hypotest_Comparisons_IN100_' method '.mat'];
    if(exist(result_file, 'file') && ~overwrite_computation)
        warning(['Result for real data exists.. Computation skipped']);
        continue;
    end
    method = Methods{mtd};
    H0_All = zeros(max(GrainID(:)),1);
    H1_All = zeros(max(GrainID(:)),1);
    Mu_All = cell(max(GrainID(:)),1);
    Mu0_All = cell(max(GrainID(:)),1);
    Kappa0_All = zeros(max(GrainID(:)),1);
    Kappa_All = zeros(max(GrainID(:)),2);
    Indexing_Split = zeros(size(Indexing));
    for g=1:size(GrainID(:),1)
        GrainID(g)
        Region = (Indexing==GrainID(g) & ~Boundary) & PatternQuality==1;
        if(sum(Region(:))<2)
            continue;
        end
        Phi = EA1(Region(:));
        Phi = [Phi EA2(Region(:))];
        Phi = [Phi EA3(Region(:))];
        RegionInd = find(Region);
        if(size(Phi,1)~=0)
            Phi(Phi<=0) = eps;
            [X, issig] = SpinCalc('EA313toQ', Phi*180/pi, 0.01, 0);
            
            switch(method)
                case 'VMF'
                    % EM-ML for mVMF
                     %%% Estimate by EM
                    [Mu_est, Kappa_est, W_est, logL0]=VMFEM_Hyper(X, Pm, 1);
                    Mu0_All{GrainID(g)} = Mu_est;
                    Kappa0_All(GrainID(g)) = Kappa_est;
                    [Mu_est, Kappa_est, W_est, logL1, CIdx]=VMFEM_Hyper(X, Pm, 2);
                    Mu_All{GrainID(g)} = Mu_est;
                    Kappa_All(GrainID(g),:) = Kappa_est(:)';
                    
                    H0_All(GrainID(g)) = logL0;
                    H1_All(GrainID(g)) = logL1;
                    %%% Clustering result
                    Indexing_Split(RegionInd(CIdx==1)) = 1;
                    Indexing_Split(RegionInd(CIdx==2)) = 2;
                case 'Watson'
                    %%% Estimate by Watson EM
                    [Mu_est, Kappa_est, W_est, logL0]=WatsonEM(X, Pm, 1);
                    Mu0_All{GrainID(g)} = Mu_est;
                    Kappa0_All(GrainID(g)) = Kappa_est;
                    [Mu_est, Kappa_est, W_est, logL1, CIdx]=WatsonEM(X, Pm, 2);
                    Mu_All{GrainID(g)} = Mu_est;
                    Kappa_All(GrainID(g),:) = Kappa_est(:)';
                    
                    H0_All(GrainID(g)) = logL0;
                    H1_All(GrainID(g)) = logL1;
                    %%% Clustering result
                    Indexing_Split(RegionInd(CIdx==1)) = 1;
                    Indexing_Split(RegionInd(CIdx==2)) = 2;
            end
        end
    end
    save([result_file], 'H0_All', 'H1_All',...
        'Kappa_All', 'Kappa0_All', 'Mu_All', 'Mu0_All', 'Indexing_Split');
end

