% The overall simulation process. Related the Figure 4 in the paper.

%% Data generation
GenerateClusteringData;

%% Parameter Estimation by EMVMF
CompareClustering;

%% Generate the figures
SimResultClusteringFigures;
